import javax.swing.*;

public class Updater extends SwingWorker<Double, Object> {

	@Override
	public Double doInBackground() {
		/*
		 * TODO
		 * 
		 * Calculator p�ld�ny l�trehoz�sa.
		 * 
		 * ciklus, am�g a calculator work() met�dusa 100-n�l kisebb �rt�kkel t�r vissza.
		 * Ezt az �rt�ket fel kell haszn�lni a progress �ll�t�s�hoz.
		 * 
		 * ha fut�s k�zben cancell�lnak (isCancelled() true), akkor meg kell szak�tani a
		 * fut�st.
		 * 
		 * A v�g�n vissza kell adni a sz�m�t�s eredm�ny�t (calculator get() met�dusa).
		 * 
		 */
		Calculator calculator = new Calculator();
		int newProgress = 0;
		int oldProgress = 0;
		while (newProgress != 100 && !isCancelled()) {
			try {
				newProgress = calculator.work();
				this.setProgress(newProgress);
				this.firePropertyChange("progress", oldProgress, newProgress);
				oldProgress = newProgress;
			} catch (InterruptedException ie) {
				// System.err.println("Interrupted at: " + newProgress);
			}
		}
		return calculator.get();
	}
}
