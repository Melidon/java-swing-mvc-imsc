import javax.swing.*;
import static javax.swing.GroupLayout.Alignment.*;
import java.awt.event.*;
import java.beans.*;
import java.lang.Exception;

public class Labor implements Runnable, ActionListener, PropertyChangeListener {

	/* a k�t updater, ha �ppen futnak */
	Updater u1;
	Updater u2;

	public Labor() {
		/* kezdetben nincs munka */
		u1 = null;
		u2 = null;
	}

	/** a gombokt�l j�v� esem�nyek kezel�se */
	@Override
	public void actionPerformed(ActionEvent e) {
		/*
		 * TODO gombnyom�s kezel�se
		 * 
		 * menet k�zben �jrakezdi a fut�st, cancell�lja a sz�lat (ha kell,
		 * interrupt-tal).
		 */

		switch (e.getActionCommand()) {
		case "button1":
			if (this.u1 != null) {
				this.u1.cancel(true);
			}
			this.u1 = new Updater();
			this.u1.addPropertyChangeListener(this);
			this.u1.execute();
			break;
		case "button2":
			if (this.u2 != null) {
				this.u2.cancel(true);
			}
			this.u2 = new Updater();
			this.u2.addPropertyChangeListener(this);
			this.u2.execute();
			break;
		default:
			System.err.println("Error: Unknown ActionCommand");
		}
	}

	/** az updater-ekt�l j�v� esem�nyek kezel�se */
	@Override
	public void propertyChange(PropertyChangeEvent e) {
		/*
		 * TODO Updater-t�l j�v� v�ltoz�sokra friss�ti a megfelel� progressbart, �s ha
		 * v�ge a fut�snak, akkor a megfelel� textfield-be be�rja az eredm�nyt
		 */
		Updater updater = null;
		JProgressBar progressBar = null;
		JTextField textField = null;
		boolean error = false;

		if (e.getSource() == this.u1) {
			updater = this.u1;
			progressBar = this.progress1;
			textField = this.field1;
		} else if (e.getSource() == this.u2) {
			updater = this.u2;
			progressBar = this.progress2;
			textField = this.field2;
		} else {
			// System.err.println("Error: Unknown Source");
			error = true;
		}

		if (!error) {
			if (e.getPropertyName().equals("progress")) {
				int progress = (Integer) e.getNewValue();
				progressBar.setValue(progress);
			} else if (e.getPropertyName().equals("state")) {
				if (updater.isDone()) {
					try {
						Double result = updater.get();
						textField.setText(result.toString());
					} catch (Exception e1) {
						// e1.printStackTrace();
					}
				}
			} else {
				System.err.println("Error: Unknown PropertyName");
			}
		}
	}

	/* az esem�nykezel�s sor�n el�rend� elemek */
	JTextField field1;
	JProgressBar progress1;
	JTextField field2;
	JProgressBar progress2;

	/** a frame-et fel�p�t� k�d */
	@Override
	public void run() {
		/* frame l�trehoz�sa */
		JFrame f = new JFrame();
		/* layout hozz�rendel�se. M�g nincs meg a kioszt�s!!! */
		GroupLayout layout = new GroupLayout(f.getContentPane());
		f.getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		/* a fels� sor elemei */
		JLabel label1 = new JLabel("First row");
		field1 = new JTextField(10);
		/*
		 * TODO gomb l�trehoz�sa, listener be�ll�t�sa
		 * 
		 */
		JButton button1 = new JButton("Update");
		button1.addActionListener(this);
		button1.setActionCommand("button1");

		progress1 = new JProgressBar(0, 99);
		/*
		 * TODO gomb l�trehoz�sa, listener be�ll�t�sa
		 * 
		 */
		JButton button2 = new JButton("Update");
		button2.addActionListener(this);
		button2.setActionCommand("button2");

		/* az als� sor elemei */
		JLabel label2 = new JLabel("Second row");
		field2 = new JTextField(10);
		progress2 = new JProgressBar(0, 99);

		/*
		 * TODO a layout a megfelel� m�don helyezi el az elemeket
		 * 
		 * 
		 */
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(LEADING).addComponent(label1).addComponent(label2))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(field1).addComponent(field2))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(button1).addComponent(button2))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(progress1).addComponent(progress2)));

		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(CENTER).addComponent(label1).addComponent(field1)
						.addComponent(button1).addComponent(progress1))
				.addGroup(layout.createParallelGroup(CENTER).addComponent(label2).addComponent(field2)
						.addComponent(button2).addComponent(progress2)));

		/* a textfield-ek nem ny�lnak f�gg�legesen */
		layout.linkSize(SwingConstants.VERTICAL, field1, field2);

		/* a frame be�ll�t�sa */
		f.pack();
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setTitle("Swing Labor");
		f.setVisible(true);
	}

	static public void main(String[] args) {
		SwingUtilities.invokeLater(new Labor());
	}
}
